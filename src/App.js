import React from 'react'
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Navbar from "./components/Navbar/Navbar";
import Home from "./pages/Home/Home";
import Quiz from "./pages/Quiz/Quiz";
import QuizState from "./context/quiz/QuizState";

function App() {
    return (
        <QuizState>
            <Router>
                <Navbar/>
                <main>
                    <Switch>
                        <Route exact path={'/'} component={Home}/>
                        <Route exact path={'/quiz/:id'} component={Quiz}/>
                    </Switch>
                </main>
            </Router>
        </QuizState>
    )
}

export default App

import React from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import FinishedQuizList from "./FinishedQuizList/FinishedQuizList";
import classes from './FinishedQuiz.module.scss'

const FinishedQuiz = ({retryQuiz, results, questions}) => {

    const resultCount = Object.keys(results).reduce((total, key) => {
        if (results[key] === 'true') {
            total++
        }
        return total
    }, 0)

    return (
        <div>
            <h1 style={{color: '#03A9F4'}}>Опрос окончен</h1>
            <FinishedQuizList
                questions={questions}
                results={results}
            />
            <button onClick={retryQuiz} className={classes.btn}>Повторить</button>
            <button style={{
                background: 'none',border: 'none', color: 'white'
            }}>
                <Link to={'/'}
                          className={classes.btn}
                          style={{
                              textDecoration: 'none', color: 'inherit'
                          }}>Перейти к тестам</Link></button>
            <h2>Вы ответили на {resultCount} ответа</h2>
        </div>
    );
};

FinishedQuiz.propTypes = {
    retryQuiz: PropTypes.func.isRequired,
    results: PropTypes.array.isRequired,
    questions: PropTypes.array.isRequired,
};

export default FinishedQuiz;

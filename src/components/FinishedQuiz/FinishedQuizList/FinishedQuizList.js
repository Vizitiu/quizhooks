import React from 'react';
import PropTypes from 'prop-types';
import classes from './FinishedQuizList.module.scss'

const FinishedQuizList = ({questions, results}) => {

    return (
        <ul className={classes.FinishedQuizList}>
            {questions.map(({question, id}) => {
                return (
                    <li key={id}
                        className={classes.FinishedQuizItem}
                        style={results[id] === 'true'
                            ? {color: 'green'}
                            : {color: 'red'}
                        }
                    >{question}</li>
                )
            })
            }
        </ul>
    );
}

FinishedQuizList.propTypes = {
    questions: PropTypes.array.isRequired,
    results: PropTypes.array.isRequired,
};

export default FinishedQuizList;

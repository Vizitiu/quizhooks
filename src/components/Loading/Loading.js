import React from 'react';
import './Loading.scss'

const Loading = () => {
    return (
            <div className="simple-spinner">
                <span/>
            </div>
    );
};

export default Loading;

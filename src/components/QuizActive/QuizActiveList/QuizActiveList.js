import React from 'react';
import PropTypes from 'prop-types';
import QuizActiveItem from "./QuizActiveItem/QuizActiveItem";
import './QuizActiveList.scss'

const QuizActiveList = ({answers, url, nextQuestion}) => (
    <ul className={'glowingList'}>
        {answers.map(({id, title}) => (
            <QuizActiveItem
                key={id}
                id={id}
                url={url}
                title={title}
                nextQuestion={nextQuestion}
            />
        ))}
    </ul>
);


QuizActiveList.propTypes = {
    answers: PropTypes.array.isRequired,
    url: PropTypes.string.isRequired,
    nextQuestion: PropTypes.func.isRequired,
};

export default QuizActiveList;

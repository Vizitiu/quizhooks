import React from 'react';
import PropTypes from 'prop-types';
import './QuizActiveItem.scss'

const QuizActiveItem = ({id, nextQuestion, url, title}) => {
    return (
        <li className={'glowingList__item'}
            onClick={() => nextQuestion(url, id)}
        >
            {title}
        </li>

    );
};

QuizActiveItem.propTypes = {
    id: PropTypes.number.isRequired,
    nextQuestion: PropTypes.func.isRequired,
    url: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
};

export default QuizActiveItem;

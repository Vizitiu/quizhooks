import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import QuizActiveList from "./QuizActiveList/QuizActiveList";

const QuizActive = ({question, answers, url, nextQuestion, questionNum}) => (
    <Fragment>
        <h3 style={{color: 'aliceblue', margin: '2rem'}}>{question.question}</h3>
        <QuizActiveList
            answers={answers}
            url={url}
            nextQuestion={nextQuestion}
        />
        {questionNum === undefined ? null : <h2 style={{
            position: 'absolute', left: '5rem', bottom: '3rem', color: 'white'
        }}>Вопрос: {questionNum + 1}</h2>}
    </Fragment>
);


QuizActive.propTypes = {
    question: PropTypes.object.isRequired,
    answers: PropTypes.array.isRequired,
    url: PropTypes.string.isRequired,
    nextQuestion: PropTypes.func.isRequired,
};

export default QuizActive;

import React from 'react';
import {useHistory} from 'react-router-dom'
import './Navbar.scss'

const Navbar = () => {
    const history = useHistory()

    const handleBack = () => {
        history.goBack()
    }

    return (
        <div className="back-button"
             onClick={handleBack}
        >
            <div className="arrow-wrap">
                <span className="arrow-part-1"/>
                <span className="arrow-part-2"/>
                <span className="arrow-part-3"/>
            </div>
        </div>
    );
};

export default Navbar;

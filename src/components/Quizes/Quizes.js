import React from 'react';
import PropTypes from 'prop-types';
import QuizesItem from "./QuizesItem/QuizesItem";
import classes from './Quizes.module.scss'

const Quizes = ({quizes}) => {
    return (
        <ul className={classes.Quizes}>
            {quizes.map(({title, id, img}) => (
                <QuizesItem title={title} id={id} key={id} img={img}/>
            ))}
        </ul>
    );
};

Quizes.propTypes = {
    quizes: PropTypes.array.isRequired
};

export default Quizes;

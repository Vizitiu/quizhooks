import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from "react-router-dom";
import classes from './QuizesItem.module.scss'

const QuizesItem = ({title, id, img}) => {
    return (
        <li style={{
            listStyle: 'none'
        }}>
            <NavLink className={classes.QuizesItem}
                     to={`/quiz/${id}`}
            >
                <span className={classes.title}>{title}</span>
                <img src={img} alt={title}
                    className={classes.img}
                />
            </NavLink>
        </li>
    );
};

QuizesItem.propTypes = {
    title: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    img: PropTypes.string.isRequired
};

export default QuizesItem;

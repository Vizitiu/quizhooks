import React, {useEffect, useContext, Fragment} from 'react';
import {QuizContext} from "../../context/quiz/QuizContext";
import QuizActive from "../../components/QuizActive/QuizActive";
import classes from './Quiz.module.scss'
import FinishedQuiz from "../../components/FinishedQuiz/FinishedQuiz";
import Loading from "../../components/Loading/Loading";

const Quiz = ({match}) => {
    const {
        getQuiz, nextQuestion, retryQuiz, results,
        quiz, question, answers, finished,
        loading, error
    } = useContext(QuizContext)
    const {title, questions} = quiz
    const url = match.params.id

    useEffect(() => {
        getQuiz(url)
    }, [])

    return (
        <Fragment>
            {error ? <p>Error</p> : null}
            {loading
                ? <Loading/>
                : <Fragment>
                    {finished
                        ? <FinishedQuiz
                            retryQuiz={retryQuiz}
                            results={results}
                            questions={questions}
                        />
                        : <Fragment>
                            <h1 className={classes.Quiz}>{title}:</h1>
                            <QuizActive question={question}
                                        questionNum={question.id}
                                        answers={answers}
                                        nextQuestion={nextQuestion}
                                        url={url}
                            />
                        </Fragment>
                    }
                </Fragment>
            }
        </Fragment>
    );
};

export default Quiz;

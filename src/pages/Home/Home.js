import React, {Fragment, useContext} from 'react';
import {QuizContext} from "../../context/quiz/QuizContext";
import Quizes from "../../components/Quizes/Quizes";
import Loading from "../../components/Loading/Loading";

const Home = () => {
    const {quizes, loading, error} = useContext(QuizContext)

    return (
        <Fragment>
            {error ? <p>Error</p> : null}
            {loading
                ? <Loading/>
                : <Quizes quizes={quizes}
                />
            }
        </Fragment>
    );
};

export default Home;

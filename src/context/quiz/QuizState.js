import React, {useReducer, useEffect} from 'react';
import {QuizContext} from "./QuizContext";
import {quizReducer} from "./quizReducer";
import axios from 'axios'
import {
    FETCH_QUIZES,
    GET_QUIZ,
    NEXT_QUIZ,
    RETRY_QUIZ,
    SET_ACTIVE_QUIZ,
    SET_ERROR,
    SET_FINISHED,
    SET_LOADING, SET_RESULTS
} from "../../types";

const STATE = {
    quizes: [],
    quiz: {},
    question: {},
    answers: [],
    quizActive: 1,
    results: [],
    loading: false,
    error: false,
    finished: false
}

const QuizState = ({children}) => {
    const [state, dispatch] = useReducer(quizReducer, STATE)

    useEffect(() => {
        const fetchQuiz = async () => {
            dispatch({type: SET_LOADING})

            try {
                const res = await axios.get('https://quizhooks.firebaseio.com/quiz.json')
                dispatch({
                    type: FETCH_QUIZES,
                    quizes: res.data
                })
            } catch (e) {
                dispatch({type: SET_ERROR})
            }
        }
        fetchQuiz()
    }, [])

    const getQuiz = async questionId => {
        dispatch({type: SET_LOADING})

        try {
            const res = await axios.get(`https://quizhooks.firebaseio.com/quiz/${questionId}.json`)
            dispatch({
                type: GET_QUIZ,
                quiz: res.data,
                question: res.data.questions[0],
                answers: res.data.questions[0].answers
            })
        } catch (e) {
            dispatch({type: SET_ERROR})
        }
    }

    const handleClick = async (questionId) => {
        dispatch({
            type: SET_ACTIVE_QUIZ,
            quizActive: quizActive + 1
        })

        try {
            const res = await axios.get(`https://quizhooks.firebaseio.com/quiz/${questionId}/questions/${quizActive}.json`)
            dispatch({
                type: NEXT_QUIZ,
                question: res.data,
                answers: res.data.answers
            })
        } catch (e) {}

    }

    const nextQuestion = (questionId, answerId) => {
        const results = state.results
        const quizLength = quiz.questions.length


        handleClick(questionId)

        if (answerId === question.rightQuiz) {
            results[question.id] = 'true'
            dispatch({
                type: SET_RESULTS,
                results
            })
        } else {
            results[question.id] = 'false'
            dispatch({
                type: SET_RESULTS,
                results
            })
        }

        if (quizLength === quizActive) {
            dispatch({type: SET_FINISHED})
        }
    }

    const retryQuiz = () => {
        dispatch({
            type: RETRY_QUIZ,
            question: quiz.questions[0],
            answers: quiz.questions[0].answers
        })
    }


    const {
        quizes, quiz, answers, quizActive,
        loading, error, question, finished, results
    } = state

    return (
        <QuizContext.Provider value={{
            getQuiz, nextQuestion, retryQuiz,
            quizes, results,
            question, finished, quiz,
            answers, loading, error
        }}>
            {children}
        </QuizContext.Provider>
    );
};

export default QuizState;

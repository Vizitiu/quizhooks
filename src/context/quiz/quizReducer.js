import {
    GET_QUIZ,
    FETCH_QUIZES,
    SET_ERROR,
    SET_LOADING,
    NEXT_QUIZ,
    SET_ACTIVE_QUIZ,
    SET_FINISHED,
    RETRY_QUIZ, SET_RESULTS
} from "../../types";

export const quizReducer = (state, {type, quizes, quiz, question, answers, quizActive, results}) => {
    switch (type) {
        case SET_LOADING:
            return {...state, loading: true, error: false}
        case SET_ERROR:
            return {...state, loading: false, error: true}
        case FETCH_QUIZES:
            return {...state, quizes, loading: false, error: false}
        case GET_QUIZ:
            return {
                ...state, quiz, quizActive: 1, question,
                answers,
                loading: false,
                error: false,
                finished: false,
                results: []
            }
        case SET_ACTIVE_QUIZ:
            return {...state, quizActive, loading: false, error: false}
        case NEXT_QUIZ:
            return {...state, question, answers, loading: false, error: false}
        case SET_FINISHED:
            return {...state, loading: false, error: false, finished: true}
        case RETRY_QUIZ:
            return {...state, question, quizActive: 1, loading: false, error: false, finished: false, results: []}
        case SET_RESULTS:
            return {...state, loading: false, error: false, results}
        default:
            return state
    }
}
